import { Component, EventEmitter, Input, Output } from '@angular/core';
import { User } from '../app.component';
import { UserService } from '../user.service';

@Component({
  selector: 'app-userlist',
  templateUrl: './userlist.component.html',
  styleUrls: ['./userlist.component.css']
})
export class UserlistComponent {

  @Input()
  users: Array<User> = [];

  @Output()
  showUserEvet = new EventEmitter<User>();

  @Output()
  upadteListEvent = new EventEmitter<void>();

  constructor(private userService: UserService) {
  }

  deleteUser(id: number) {
    this.userService.removeUser(id).subscribe({
      next: (response) => {
        console.log("User has been removed");
        this.upadteListEvent.emit();
      }
    })
  }

  showUser(id: number) {
    this.userService.getUser(id).subscribe({
      next: (user) => this.showUserEvet.emit(user)
    })
  }
}
