import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { User } from './app.component';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  base_url = 'http://localhost:8080/api/users'

  constructor(private httpClient: HttpClient) { }

  getUserList() {
    return this.httpClient.get<Array<User>>(this.base_url);
  }
  
  getUser(id: number) {
    return this.httpClient.get<User>(this.base_url + "/" + id);
  }

  removeUser(id: number) {
    return this.httpClient.delete(this.base_url + "/" + id);
  }

  sendUser(user: User) {
    let header = new HttpHeaders({'content-type': 'application/json'})
    return this.httpClient.post(this.base_url, user, {headers: header})
  }
}
