import { Component } from '@angular/core';
import { UserService } from './user.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'angular-api-consumer';
  selectedUser?: User;
  users: Array<User> = [];

  constructor(private userService: UserService) {
    this.updateUserList();
  }

  selectUser(user: User) {
    this.selectedUser = user;
  }

  updateUserList() {
    this.userService.getUserList().subscribe({
      next: (response) => this.users = response,
      error: () => console.error("Error: Not able to get user list")
    })
  }
}

export interface User {
  id: number;
  firstName: string;
  lastName: string;
  age: number;
}