import { Component, EventEmitter, Output } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { User } from '../app.component';
import { UserService } from '../user.service';

@Component({
  selector: 'app-newuser',
  templateUrl: './newuser.component.html',
  styleUrls: ['./newuser.component.css']
})
export class NewuserComponent {

  constructor(private userService: UserService, private formBuilder: FormBuilder){}

  @Output()
  upadteListEvent = new EventEmitter<void>();

  userForm = this.formBuilder.group({
    firstName: ['', {
      validators: [
        Validators.required,
        Validators.minLength(2),
        Validators.maxLength(100),
        Validators.pattern("^[A-Ża-ż]+$")
      ],
      updateOn: 'blur'
    }],
    lastName: ['', 
    {
      validators: [
        Validators.required,
        Validators.maxLength(100),
        Validators.minLength(2),
        Validators.pattern("^[A-Ża-ż]+$")
      ],
      updateOn: 'blur'
    }],
    age: [0, 
    {
      validators: [
        Validators.required,
        Validators.max(150),
        Validators.min(0),
      ],
      updateOn: 'blur'
    }]
  })

  validation_messages = {
    'firstName': [
      {type: 'required', message: 'Imię jest wymagane'},
      {type: 'pattern', message: 'Tylko litery są dozwolone'},
      { type: 'minlength', message: 'Imie jest za krótkie. (min 2 znaki)' },
      { type: 'maxlength', message: 'Imię jest za długie (max 100 znaków)' },
    ],
    'lastName': [
      { type: 'required', message: 'Nazwisko jest wymagane' },
      {type: 'pattern', message: 'Tylko litery są dozwolone'},
      { type: 'minlength', message: 'Nazwisko jest za krótkie. (min 2 znaki)' },
      { type: 'maxlength', message: 'Nazwisko jest za długie (max 100 znaków)' },
    ],
    'age': [
      { type: 'required', message: 'Wiek jest wymagany' },
      { type: 'max', message: 'Wiek nie może być większy niż 150' },
      { type: 'min', message: 'Wiek nie może byc mniejszy niż 0' },
    ]
  }

  get firstName() { return this.userForm.get('firstName'); }
  get lastName() { return this.userForm.get('lastName'); }
  get age() { return this.userForm.get('age'); }

  submitUserForm(){
    if(this.userForm.valid){
      this.userService.sendUser(this.convertToUserForm()).subscribe({
        next: (response) => {
          this.userForm.reset();
          this.upadteListEvent.emit();
        }
      })

    } else {
      console.error("Form not valid")
    }
  }

  private convertToUserForm() {
    return {
      firstName: this.firstName?.value,
      lastName: this.lastName?.value,
      age: this.age?.value
    } as User;
  }

}
