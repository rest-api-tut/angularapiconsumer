import { Component, Input } from '@angular/core';
import { User } from '../app.component';

@Component({
  selector: 'app-showuser',
  templateUrl: './showuser.component.html',
  styleUrls: ['./showuser.component.css']
})
export class ShowuserComponent {

  @Input()
  selectedUser?: User;
}
